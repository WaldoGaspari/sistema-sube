Una empresa de telecomunicaciones requiere generar facturas a sus clientes por el abono del uso de su servicio, teniendo en cuenta que:

    La facturación se realiza de manera mensual
    La facturación está compuesta por:
        Un abono mensual básico de $100
        Consumo por llamadas Locales, Nacionales e Internacionales
    El sistema de facturacion recibe las llamadas realizadas por cada cliente con los siguientes datos:
        Nro de origen (cod pais + cod area + nro)
        Nro de destino (cod pais + cod area + nro)
        Fecha y hora de inicio de llamada
        Fecha y hora de fin de llamada
    El código de pais son 2 digitos, el código de area son 3 digitos y el numero son 8 digitos
    Las llamadas locales tienen distintos valores según la franja horaria en la que se realizan y el día. 
        Para los días hábiles, de 8 a 20 hrs. el costo es de $3,20 el minuto
        Para el resto del día hábil es de $1,80 el minuto. 
        Los sábados y domingos cuesta $2,10 el minuto
    Las llamadas Nacionales tienen un costo de 
        $20 las llamadas de menos de 5 minutos
        $1.50 el minuto adicional
    Las llamadas Internacionales tienen un costo distinto según el país al que se llame. 
        Para los países de Sudamérica $6 el minuto, 
        Para Norteamerica $10 el minuto.
        Para el resto del mundo $15  el minuto.

A partir de esto se pide implementar las siguientes dos funcionalidades:

    POST /llamadas
    { "numero_origen": "541140895555",
    "numero_destino": "541134561221",
    "fechahora_inicio": "2019-01-01;22:30", "fechahora_fin":"2019-01-01;22:34"}

    GET /facturacion?numero=541140895555&mes=201901




[![pipeline status](https://gitlab.com/WaldoGaspari/facturador-de-llamadas/badges/master/pipeline.svg)](https://gitlab.com/WaldoGaspari/facturador-de-llamadas/commits/master)

Alumno: Waldo Gaspari
